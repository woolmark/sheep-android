Sheep for Android
----------------------------------------------------------------------------
Sheep for Android is an implementation [sheep] for [android].

Get it on Google Play
----------------------------------------------------------------------------
[![Get it on Google Play][get_it_logo]](https://play.google.com/store/apps/details?id=org.bitbucket.woolmark.sheep.android)

License
-----------------------------------------------------------------------------
[WTFPL] 

[sheep]: https://woolmark.bitbucket.org/ "Sheep"
[android]: http://www.android.com "Android"
[get_it_logo]: https://developer.android.com/images/brand/en_generic_rgb_wo_45.png
[get_it]: https://play.google.com/store/apps/details?id=org.bitbucket.woolmark.sheep.android
[WTFPL]: http://www.wtfpl.net "WTFPL"

