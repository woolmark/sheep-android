package org.bitbucket.woolmark.sheep.android;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * sheep activity.
 * @author takimura
 */
public class SheepActivity extends Activity implements SheepView.OnSheepCountUpListener {

    private static final String PREF_SHEEP = "sheep";

    private static final String KEY_SHEEP_NUMBER = "sheep_number";

    private SheepView mSheepView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mSheepView = (SheepView) findViewById(R.id.sheep_view);
        mSheepView.setOnSheepCountUpListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mSheepView != null) {
            mSheepView.setSheepCount(loadSheepCount(this));
        }

        onSheepCountUp(mSheepView.getSheepCount());
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mSheepView != null) {
            saveSheepCount(this, mSheepView.getSheepCount());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
        case R.id.zz:
            finish();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSheepCountUp(final int number) {
        TextView counter = (TextView) findViewById(R.id.sheep_counter);
        counter.setText(getString(R.string.sheep_count, number));
    }

    private int loadSheepCount(final Context context) {

        int count = 0;

        SharedPreferences prefs = context.getSharedPreferences(PREF_SHEEP, Context.MODE_PRIVATE);
        if (prefs != null) {
            count = prefs.getInt(KEY_SHEEP_NUMBER, 0);
        }

        return count;
    }

    private void saveSheepCount(final Context context, final int count) {

        SharedPreferences prefs = context.getSharedPreferences(PREF_SHEEP, Context.MODE_PRIVATE);
        if (prefs != null) {
            prefs.edit().putInt(KEY_SHEEP_NUMBER, count).commit();
        }

    }

}
